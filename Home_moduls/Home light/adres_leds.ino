#include "FastLED.h"
const short NUM_LEDS = 5; // set all leds
const short PIN = 6;      // set pin light
CRGB leds[NUM_LEDS];

void setup() {
  Serial.begin(9600);
  FastLED.setMaxPowerInVoltsAndMilliamps(5, 300); // volte and ampers
  FastLED.addLeds<WS2811, PIN, GRB>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
  FastLED.setBrightness(150); // set highest light
}
void loop() {
  for (int i = 0; i < NUM_LEDS; ++i) {
    FastLED.clear();
    leds[i].setHue(134);
    FastLED.show();
    delay(300);
  }
  for (int j = NUM_LEDS; j > 0; --j) {
    FastLED.clear();
    leds[j].setHue(134);
    FastLED.show();
    delay(300);
  }
}
