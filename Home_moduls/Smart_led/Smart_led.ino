#include "IRremote.h" 
IRrecv irrecv(2); 
decode_results results;
boolean X_flag, Z_flag, Y_flag;  

void setup() {
  irrecv.enableIRIn(); // запускаем прием инфракрасного сигнала
  Serial.begin(9600); 
  pinMode(2, INPUT);   // пин ИК приемника
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
}

void loop() {
   if (irrecv.decode(&results)) {  // если данные пришли
    Serial.println(results.value); // отправляем полученные данные на порт
    
    switch(results.value) {        // обработчик нажатий         
      case 16:
       X_flag = 1;
       digitalWrite(9, X_flag);
     break;
      case 2064:
       Z_flag = 1;
       digitalWrite(10, Z_flag);
     break;
      case 1040:
       Y_flag = 1;
       digitalWrite(11, Y_flag);
     break;
  } 

  if (results.value == 2704) { 
    digitalWrite(9, LOW);
    digitalWrite(10, LOW);
    digitalWrite(11, LOW);
  }
   irrecv.resume(); // принимаем следующий сигнал на ИК приемнике
  }
}
