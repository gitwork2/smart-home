void address_effect_one() {
  pixels.clear();
  for (int i = 0; i < NUM_LEDS; i++) { // For each pixel...
      pixels.setPixelColor(i, pixels.Color(252, 252, 252));
      pixels.show();
      delay(200);
  }
}

void color_while() {
  pixels.clear();
  for (int n = 0; n < 3; ++n) {
    for (int color = 0; color < 256; ++color) {
      for (int i = 0; i < NUM_LEDS; ++i) {
        if (n == 0) {
          pixels.setPixelColor(i, pixels.Color(color, 0, 0));
        } else if (n == 1) {
          pixels.setPixelColor(i, pixels.Color(0, color, 0));
        } else if (n == 2) {
          pixels.setPixelColor(i, pixels.Color(0, 0, color));
        }
        pixels.show();
      }
    }
  }
}
