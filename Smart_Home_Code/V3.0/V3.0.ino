// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= //
// Smart_Home_Code
// V3.0
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= //

// here save ik signals
enum {
  RIGHT   = 16761405, // right(switch >)
  LEFT    = 16720605, // left (switch <)
  SRV_MAX = 16736925, // low  (switch v)
  SRV_MIN = 16754775, // high (switch ^)
  RESET   = 16732845, // reboot arduino
  SS_PIN   = 10,      // rfid pin
  RST_PIN  = 9,       // rfid pin
  NUM_LEDS = 15,      // set all leds
  LEDS_PIN = A2,      // set pin ik
  ECHO_PIN = 5,       // set echo pin for sonar
  TRIGGER_PIN = 4,    // set trigger pin for sonars
  SWITCH_ZERO   = 16730805, // turn on nigth mod
  SWITCH_ONE    = 16738455, // first  led effect
  SWITCH_TWO    = 16750695, // second led effect
  SWITCH_THREE  = 16756815, // thour  led effect
  SWITCH_FOUR   = 16724175, // fan
  SWITCH_FIVE   = 16718055,
  SWITCH_SIX    = 16743045,
  SWITCH_SEVEN  = 16716015,
  SWITCH_EIGTH  = 16726215,
  SWITCH_NINE   = 16734885,
  ADDRESS_LED_OFF = 16728765
};

//======== БИБИЛИОТЕКИ =========//
#include <SPI.h>
#include <MFRC522.h>  // for rfid 
#include <DHT.h>      // for dht 11
#include <Servo.h>    // for servo
#include <IRremote.h> // for ik
#include <LiquidCrystal_I2C.h>
#include <Adafruit_NeoPixel.h>
Adafruit_NeoPixel pixels(NUM_LEDS, LEDS_PIN, NEO_GRB + NEO_KHZ800);
#include "Address_led_effects.h"
#include <NewPing.h>

NewPing sonar(TRIGGER_PIN, ECHO_PIN, 300);
MFRC522 rfid(SS_PIN, RST_PIN); // Instance of the class
MFRC522::MIFARE_Key key; // rfid
LiquidCrystal_I2C lcd(0x27, 16, 2);
decode_results results;  // ik
DHT dht(8, DHT11); // pin dht
IRrecv irrecv(2);   // pin ik sensor
Servo servo;

//======== НАСТРОЙКИ/ПЕРЕМЕННЫЕ ========//
byte nuidPICC[4]; // for rfid librery
static bool flag_arr[15];
static bool butt_arr[15];
uint32_t tmr;

void setup() {
  Serial.begin(9600);
  lcd.init();
  lcd.backlight();
  pinMode(2, INPUT); // pin ik
  // set pins leds and fan
  pinMode(6, OUTPUT);
  servo.attach(3); // pin servo
  servo.write(0);  // set servo
  irrecv.enableIRIn();
  SPI.begin();     // Init SPI bus
  rfid.PCD_Init(); // Init MFRC522
  dht.begin();     // begin DHT11
  pixels.begin();  // INITIALIZE NeoPixel strip object (REQUIRED)
  pixels.show();
  
  for (byte i = 0; i < 6; i++) {
    key.keyByte[i] = 0xFF;
  }
}

byte i = 0;
bool data_flag;

void yield() {
  if (results.value == RESET) { asm volatile("jmp 0x00"); } // reset arduino if press '#'
  if (results.value == ADDRESS_LED_OFF) { // clean all address led
    pixels.clear();
    for (int i = 0; i < NUM_LEDS; i++) { // For each pixel...
      pixels.setPixelColor(i, pixels.Color(0, 0, 0));
      pixels.show();
    }
  }
  
  if (rfid.uid.uidByte[0] == 91 &&
    rfid.uid.uidByte[1] == 40 &&
    rfid.uid.uidByte[2] == 112 &&
    rfid.uid.uidByte[3] == 139) {
    // ================================= SKREEN FUNCTIONS ================================= //
    if (data_flag == false) {
      infoScreen();
    } else {
      dhtSkreen();
    }
    // ======================================= SWITCH ==================================== //
    if (irrecv.decode(&results)) {   // если данные пришли выполняем команды
      Serial.println(results.value); // отправляем полученные данные на порт
      if (millis() - tmr > 80) {     // обработчик нажатий c ik
        switch (results.value) {
          case SWITCH_FOUR:
            butt_arr[0] = true; // fan
            i = 0;
            break;
          case SWITCH_ONE: // first led effect
            butt_arr[1] = true;
            i = 1;
            break;
          case SWITCH_TWO: // second led effect
            butt_arr[4] = true;
            i = 4;
            break;
          case SWITCH_THREE: // third led effect
            butt_arr[5] = true;
            i = 5;
            break;
          case SWITCH_ZERO:  // turn on nigth mod 
            butt_arr[6] = true;
            i = 6;
            lcd.clear(); 
            break;
          case RIGHT:
            lcd.clear();
            data_flag = true;
            break;
          case LEFT:
            lcd.clear();
            data_flag = false;
            break;
        }
        tmr = millis();
      }

// ============================= SERVO ============================ //
    if (results.value == SRV_MAX) {
      servo.write(180); // open
      butt_arr[2] = true;
      i = 2;
    }
    if (results.value == SRV_MIN) {
      servo.write(0); // close
      butt_arr[3] = true;
      i = 3;
    }
    irrecv.resume(); // принимаем следующий сигнал на ik приемнике
  }
// ==================================== ALGORITMS =============================== //
    bool flag;
    // Имитация отпускания
    if (millis() - tmr > 80) {
      tmr = millis();
      butt_arr[i] = 0;
    }
    // Алгоритм фиксированой кнопки
    if (butt_arr[i] == true && flag == false) {
      flag_arr[i] = !flag_arr[i];
      flag = true;
      lcd.clear();
    }
    if (butt_arr[i] == false && flag == true) {
      flag = false;
    }

    if (flag_arr[0] == true) { // turn on/off fan
      analogWrite(6, 60);
    } else {
      digitalWrite(6, LOW);
    }
  }
}

void loop() {
  rfidScan();
  if (rfid.uid.uidByte[0] == 91 &&
    rfid.uid.uidByte[1] == 40   &&
    rfid.uid.uidByte[2] == 112  &&
    rfid.uid.uidByte[3] == 139) {
      
    if (flag_arr[6] == true) { // switch zero
      lcd.setCursor(0, 1);
      lcd.print("Night mod");
      if (sonar.ping_cm() < 20) {
        address_effect_one();
      }
    }
    
    if (flag_arr[1] == true) {       // switch one
      lcd.setCursor(0, 1);
      lcd.print("Light: on");
      address_effect_one();      
      flag_arr[1] = false;
   } else if (flag_arr[4] == true) { // switch two
      lcd.setCursor(0, 1);
      lcd.print("Light: cycle");
      color_while();
      flag_arr[4] = false;
   } else if (flag_arr[5] == true) { // switch three
      lcd.setCursor(0, 1);
      lcd.print("Light:");
      flag_arr[5] = false;
    } else {
      lcd.clear();
      lcd.setCursor(0, 1);
      lcd.print("Light: off");
    }
  }
}

// ====================================== LCD_INFO_SCREEN ================================ //
void infoScreen() {
  String lcd_work = "";
  if (servo.read() < 100) lcd_work = "FAN:          ||"; // if garage is close
  if (servo.read() > 170) lcd_work = "FAN:          /|"; // if garage is open
  if (flag_arr[0] == true) {
    lcd_work[6] = 'O';
    lcd_work[7] = 'N';
  } else {
    lcd_work[6] = 'O';
    lcd_work[7] = 'F';
    lcd_work[8] = 'F'; 
  }
  lcd.setCursor(0, 0);
  lcd.print(lcd_work);
}

void dhtSkreen() {
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  lcd.setCursor(0, 0);
  lcd.print("Humid:");
  lcd.setCursor(7, 0);
  lcd.print(h);
  lcd.print("%");

  lcd.setCursor(0, 1);
  lcd.print("Tempe: ");
  lcd.setCursor(7, 1);
  lcd.print(t);
  lcd.print("*C");
}

// ========================================== RFID_FUNCTION ================================= //
void rfidScan() {
  // Reset the loop if no new card present on the sensor/reader.
  if (!rfid.PICC_IsNewCardPresent()) { return; }
  // Verify if the NUID has been readed
  if (!rfid.PICC_ReadCardSerial()) { return; }

  Serial.print(F("PICC type: "));
  MFRC522::PICC_Type piccType = rfid.PICC_GetType(rfid.uid.sak);
  Serial.println(rfid.PICC_GetTypeName(piccType));

  // Check is the PICC of Classic MIFARE type
  if (piccType != MFRC522::PICC_TYPE_MIFARE_MINI &&
      piccType != MFRC522::PICC_TYPE_MIFARE_1K &&
      piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
    Serial.println(F("Your tag is not of type MIFARE Classic."));
    return;
  }
  // Check new card
  if (rfid.uid.uidByte[0] != nuidPICC[0] ||
      rfid.uid.uidByte[1] != nuidPICC[1] ||
      rfid.uid.uidByte[2] != nuidPICC[2] ||
      rfid.uid.uidByte[3] != nuidPICC[3] ) {
    Serial.println(F("A new card has been detected."));

    // Store NUID into nuidPICC array
    for (byte i = 0; i < 4; i++) {
      nuidPICC[i] = rfid.uid.uidByte[i];
    }
    // Serial print a NUID new card
    Serial.print(F("In dec: "));
    printDec(rfid.uid.uidByte, rfid.uid.size);
    Serial.println();
  }
  else Serial.println(F("Card read previously."));

  // Halt PICC
  rfid.PICC_HaltA();
  // Stop encryption on PCD
  rfid.PCD_StopCrypto1();
}

//Helper routine to dump a byte array as dec values to Serial.
void printDec(byte *buffer, byte bufferSize) {
  for (byte v = 0; v < bufferSize; v++) {
    Serial.print(buffer[v] < 0x10 ? " 0" : " ");
    Serial.print(buffer[v], DEC);
  }
}
