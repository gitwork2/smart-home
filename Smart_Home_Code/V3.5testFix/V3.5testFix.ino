// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= //
// Smart_Home_Code
// https://gitlab.com/gitwork2/smart-home
// V3.5
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= //

enum IkCodes { // here save ik signals
  RIGHT        = 3280, // right(switch >)
  LEFT         = 720,  // left (switch <)
  SRV_MAX      = 752,  // low  (switch v)
  SRV_MIN      = 2800, // high (switch ^)
  RESET        = 3184, // reboot arduino
  SWITCH_ZERO  = 2320, // turn on nigth mod
  SWITCH_ONE   = 16,   // first  led effect
  SWITCH_TWO   = 2064, // second led effect
  SWITCH_THREE = 1040, // thour  led effect
  SWITCH_FOUR  = 3088, // fan
  SWITCH_FIVE  = 528,
  SWITCH_SIX   = 2576,
  SWITCH_SEVEN = 1552,
  SWITCH_EIGTH = 3600,
  SWITCH_NINE  = 272,
  ADDRESS_LED_OFF = 2704,   // clear whole led
  NUM_LEDS     = 15, // whole leds
  SONAR_DIST   = 50  // max dastance in nigth mod (cm)
};

enum Pins {
  SS_PIN    = 10, // rfid pin
  RST_PIN   = 9,  // rfid pin
  LEDS_PIN  = A2, // set pin ik
  ECHO_PIN  = 5,  // set echo pin for sonar
  TRIG_PIN  = 4,// set trigger pin for sonars
  DHT_PIN   = 8,  // dht pin
  IK_PIN    = 2,  // ik pin
  FAN_PIN   = 6,  // pin of fan
  SERVO_PIN = 3,  // servo pin
};

//======== БИБИЛИОТЕКИ =========//
#include <SPI.h>
#include <DHT.h>
#include <Servo.h>
#include <MFRC522.h>
#include <NewPing.h>
#include "IRremote.h"
#include <LiquidCrystal_I2C.h>
#include <Adafruit_NeoPixel.h>
Adafruit_NeoPixel pixels(NUM_LEDS, LEDS_PIN, NEO_GRB + NEO_KHZ800);
#include "Address_led_effects.h"

NewPing sonar(TRIG_PIN, ECHO_PIN, 300);
LiquidCrystal_I2C lcd(0x27, 16, 2);
MFRC522 rfid(SS_PIN, RST_PIN);
MFRC522::MIFARE_Key key;
DHT dht(DHT_PIN, DHT11); // pin dht
decode_results results;
IRrecv irrecv(IK_PIN);   // pin ik sensor
Servo servo;


//======== НАСТРОЙКИ/ПЕРЕМЕННЫЕ ========//
byte nuidPICC[4]; // rfid key saver
static bool flag_arr[15];
static bool butt_arr[15];
uint32_t tmr; // for millis timer

void setup() {
  srand(analogRead(A6));
  lcd.init();
  lcd.backlight();
  pinMode(IK_PIN,  INPUT);
  pinMode(FAN_PIN, OUTPUT);
  servo.attach(SERVO_PIN);
  servo.write(0);
  irrecv.enableIRIn(); // init ik
  SPI.begin();         // Init SPI bus
  rfid.PCD_Init();     // Init MFRC522
  dht.begin();         // begin DHT11
  pixels.begin();      // INITIALIZE NeoPixel strip object (REQUIRED)
  pixels.show();
  
  Serial.begin(9600);
  for (byte i = 0; i < 6; i++) {
    key.keyByte[i] = 0xFF;
  }
}

void yield() {
  static bool data_flag;
  static byte i = 0;
  // reset arduino if press "#"
  if (results.value == RESET) { asm volatile("jmp 0x00"); }
  if (results.value == ADDRESS_LED_OFF) { // clean all address led
    pixels.clear();
    for (int i = 0; i < NUM_LEDS; i++) {  // For each pixel...
      pixels.setPixelColor(i, pixels.Color(0, 0, 0));
      pixels.show();
    }
  }
  
  if (rfid.uid.uidByte[0] == 91 &&
    rfid.uid.uidByte[1] == 40 &&
    rfid.uid.uidByte[2] == 112 &&
    rfid.uid.uidByte[3] == 139) {
// ================================= SKREEN FUNCTIONS ================================= //
    if (data_flag == false) {
      infoScreen();
    } else {
      dhtSkreen();
    }
// ======================================= SWITCH ==================================== //
    if (irrecv.decode(&results)) {   // если данные пришли выполняем команды
      Serial.println(results.value); // отправляем полученные данные на порт
      if (millis() - tmr > 80) {     // обработчик нажатий c ik
        switch (results.value) {
          case SWITCH_FIVE:  // fan
            butt_arr[10] = true;
            i = 10;
            break;
          case SWITCH_ONE:   // first led effect
            butt_arr[1] = true;
            i = 1;
            break;
          case SWITCH_TWO:   // second led effect
            butt_arr[4] = true;
            i = 4;
            break;
          case SWITCH_THREE: // third led effect
            butt_arr[5] = true;
            i = 5;
            break;
          case SWITCH_FOUR:  // fourty led effect
            butt_arr[9] = true;
            i = 9;
            break;
          case SWITCH_ZERO:  // turn on nigth mod
            butt_arr[6] = true;
            i = 6;
            break;
          case RIGHT:
            data_flag = true;
            break;
          case LEFT:
            data_flag = false;
            break;
        }
        tmr = millis();
      }

// ============================= SERVO ============================ //
    if (results.value == SRV_MAX) {
      servo.write(180); // open
    }
    if (results.value == SRV_MIN) {
      servo.write(0);   // close
    }
    irrecv.resume(); // принимаем следующий сигнал на ik приемнике
  }
// ==================================== ALGORITMS =============================== //
    // Имитация отпускания
    if (millis() - tmr > 80) {
      tmr = millis();
      butt_arr[i] = false;
    }
    
    static bool flag;
    // Алгоритм фиксированой кнопки
    if (butt_arr[i] == true && flag == false) {
      flag_arr[i] = !flag_arr[i];
      flag = true;
    }
    if (butt_arr[i] == false && flag == true) {
      flag = false;
    }
 
    if (flag_arr[10] == true) { // turn on/off fan
      analogWrite(FAN_PIN, 40);
    } else {
      digitalWrite(FAN_PIN, LOW);
    }
  }
}

void loop() {
  rfidScan();
  if (rfid.uid.uidByte[0] == 91 &&
    rfid.uid.uidByte[1] == 40   &&
    rfid.uid.uidByte[2] == 112  &&
    rfid.uid.uidByte[3] == 139) {

    lcd.setCursor(0, 1);
    if (flag_arr[1] == true) {       // switch one
      lcd.print("Light: on         ");
      address_effect_one();
      flag_arr[1] = false;
   } else if (flag_arr[4] == true) { // switch two
      lcd.print("Light: cycle      ");
      infoScreen();
      color_while();
      flag_arr[4] = false;
   } else if (flag_arr[5] == true) { // switch three
      lcd.print("Light: play mod   ");
      play_mod(tmr);
      flag_arr[5] = false;
   } else if (flag_arr[9] == true) { // switch three
      lcd.print("Light: party      ");
      crazy_led(tmr);
      flag_arr[9] = false;     
   } else if (flag_arr[6] == true) { // switch zero
      lcd.setCursor(0, 1);
      lcd.print("Night mod        ");
      if (sonar.ping_cm() < SONAR_DIST) {
        address_effect_one();
      }
   } else {
      lcd.print("Light: off    ");
    }
  }
}

// ====================================== LCD_INFO_SCREEN ================================ //
void infoScreen() {
  String lcd_work = "";
  if (servo.read() < 100) lcd_work = "FAN:          ||"; // if garage is close
  if (servo.read() > 170) lcd_work = "FAN:          /|"; // if garage is open
  if (flag_arr[10] == true) {
    lcd_work[7] = 'o';
    lcd_work[8] = 'n';
  } else {
    lcd_work[7] = 'o';
    lcd_work[8] = 'f';
    lcd_work[9] = 'f'; 
  }
  lcd.setCursor(0, 0);
  lcd.print(lcd_work);
}

void dhtSkreen() {
  lcd.clear();
  float h = dht.readHumidity();    // get humid
  float t = dht.readTemperature(); // get temperature
  lcd.setCursor(0, 0);
  lcd.print("Humid:");
  lcd.setCursor(7, 0);
  lcd.print(h); // print humid
  lcd.print("%");

  lcd.setCursor(0, 1);
  lcd.print("Tempe: ");
  lcd.setCursor(7, -1);
  lcd.print(t); // print temperature
  lcd.print("*C");
  lcd.print("   "); // Ограничение символов
  delay(10);
}

// ========================================== RFID_FUNCTION ================================= //
void rfidScan() {
  // Reset the loop if no new card present on the sensor/reader.
  if (!rfid.PICC_IsNewCardPresent()) { return; }
  // Verify if the NUID has been readed
  if (!rfid.PICC_ReadCardSerial()) { return; }

  Serial.print(F("PICC type: "));
  MFRC522::PICC_Type piccType = rfid.PICC_GetType(rfid.uid.sak);
  Serial.println(rfid.PICC_GetTypeName(piccType));

  // Check is the PICC of Classic MIFARE type
  if (piccType != MFRC522::PICC_TYPE_MIFARE_MINI &&
      piccType != MFRC522::PICC_TYPE_MIFARE_1K &&
      piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
    Serial.println(F("Your tag is not of type MIFARE Classic."));
    return;
  }
  // Check new card
  if (rfid.uid.uidByte[0] != nuidPICC[0] ||
      rfid.uid.uidByte[1] != nuidPICC[1] ||
      rfid.uid.uidByte[2] != nuidPICC[2] ||
      rfid.uid.uidByte[3] != nuidPICC[3] ) {
    Serial.println(F("A new card has been detected."));

    // Store NUID into nuidPICC array
    for (byte i = 0; i < 4; i++) {
      nuidPICC[i] = rfid.uid.uidByte[i];
    }
    // Serial print a NUID new card
    Serial.print(F("In dec: "));
    printDec(rfid.uid.uidByte, rfid.uid.size);
    Serial.println();
  }
  else Serial.println(F("Card read previously."));

  // Halt PICC
  rfid.PICC_HaltA();
  // Stop encryption on PCD
  rfid.PCD_StopCrypto1();
}

//Helper routine to dump a byte array as dec values to Serial.
void printDec(byte *buffer, byte bufferSize) {
  for (byte v = 0; v < bufferSize; v++) {
    Serial.print(buffer[v] < 0x10 ? " 0" : " ");
    Serial.print(buffer[v], DEC);
  }
}
