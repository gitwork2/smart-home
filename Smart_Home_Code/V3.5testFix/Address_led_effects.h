void address_effect_one() {
  pixels.clear();
  for (int i = 0; i < NUM_LEDS; i++) { // For each pixel...
      pixels.setPixelColor(i, pixels.Color(random(0, 255), random(0, 255), random(0, 255)));
      pixels.show();
      delay(150);
  }
}

void color_while() {
  pixels.clear();
  for (int n = 0; n < 3; ++n) {
    for (int color = 0; color < 256; ++color) {
      for (int i = 0; i < NUM_LEDS; ++i) {
        if (n == 0) {
          pixels.setPixelColor(i, pixels.Color(color, 0, 0));
        } else if (n == 1) {
          pixels.setPixelColor(i, pixels.Color(0, color, 0));
        } else if (n == 2) {
          pixels.setPixelColor(i, pixels.Color(0, 0, color));
        }
        pixels.show();
      }
    }
  }
}

void play_mod(uint32_t tmr) {
  byte n = 0;
  while (n < 3) {
  pixels.clear();
  if (millis() - tmr > 100) {
    tmr = millis();
    int pixNum = 0;
    int randRed   = random(0, 255);
    int randBlue  = random(0, 255);
    int randGreen = random(0, 255);
    int bright = 255;
    for (int d = 10; d < 600; ++d) {
      if (d < 255) { pixels.setBrightness(d); }
      if (d > 255) {
        --bright;
        pixels.setBrightness(bright);
      }
      while (pixNum < 15) {
        pixels.setPixelColor(++pixNum, pixels.Color(random(0, 255), random(0, 255), random(0, 255)));
      }
        delay(1);
        pixels.show();
      }
    }
    ++n;
  }
}

void crazy_led(uint32_t tmr) {
  while (true) {
  pixels.clear();
  if (millis() - tmr > 100) {
    tmr = millis();
    int pixNum = 0;
    while (pixNum < 15) {
      pixels.setBrightness(random(150, 255));
      pixels.setPixelColor(++pixNum, pixels.Color(random(0, 255), random(0, 255), random(0, 255)));
    }
      pixels.show();
    }
  }
}
